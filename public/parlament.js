let Relational = require('relational.js');
let { Relation } = Relational;
let { gt, gte, lt, lte } = Relational;
let { multiply, plus, subtract } = Relational;


//print

function taula_cens(taula){
  taula_pesos = pesos(escons);

  header = {circumscripcio: "Circumscripció", escons: "Escons", cens: "Cens", pes: "Pes diferencial"}
  tablehtml = relation2html(taula.and(taula_pesos), header)
  document.getElementById("cens").appendChild(tablehtml);
}

function taula_votacions(taula){
  header = {circumscripcio: "Circumscripció", partit: "Partit", vots: "Escons"}
  tablehtml = relation2html(taula, header)
  document.getElementById("resultats").appendChild(tablehtml);
}



//utils

function csv2relationVots(text){
  //Retorna una relation a partir d'un text CSV
  function parse_line(line){
    values = line.split('\t');
    return {circumscripcio: values[0], partit: values[1], vots: Number(values[2])};
  }

  rows = text.split('\n')
  return new Relation(null, rows.map(parse_line, rows));
}

function csv2relationCens(text){
  //Retorna una relation a partir d'un text CSV
  function parse_line(line){
    values = line.split('\t');
      return {circumscripcio: values[0], escons: Number(values[1]), cens: Number(values[2])};
  }

  rows = text.split('\n')
  return new Relation(null, rows.map(parse_line, rows));
}

function relation2html(relation, header_print){
  //Escriu la relació a HTML table

  table = document.createElement("table");
  table.className = "w3-table-all w3-hoverable";
  header = Object.keys(relation.heading)

  // heading
  thead = document.createElement("thead");
  trh = document.createElement("tr");
  table.appendChild(thead)
  thead.appendChild(trh)

  function heading(text){
    th = document.createElement("th");
    trh.appendChild(th)

    if (header_print){
	t = document.createTextNode(header_print[text]);
    } else {
	t = document.createTextNode(text);
    };
    th.appendChild(t);
  }

  header.forEach(heading);

  // body
  function row(tuple){
    tr = document.createElement("tr");
    table.appendChild(tr)

    for (i = 0; i < header.length; i++) {
      td = document.createElement("td");
      tr.appendChild(td)

      t = document.createTextNode(tuple[header[i]]);
      td.appendChild(t)
    }

  }

  relation.forEach(row);

  return table;

}


function summarize_by_sum(rel, attr){
   // summarize rel BY ADD(attr)
   function suma(total, tup) {
      return total + tup[attr];
   }
  return Array.from(rel.values()).reduce(suma, 0);
}



//extres

function ajunta_partits(votacio, partits, partit_nou){
    //Ajunta partits en un partit_nou per cada circumscripció de la votació
    let rel_partit= new Relation(null, partits.map(function taula(i){ return {partit: i};}) );

  //summarize subresultat by sum(vots) PER circumscripcio,partits
  function summarise(prev, rel){
  let circumscripcio_c = rel['circumscripcio'];
  // select resultat where circumscripcio = 'circumscripcio'
  let rel_sel = new Relation(null, [{circumscripcio: circumscripcio_c}]);
  let subresultat = rel_sel.and(votacio)
  let nopartits = rel_partit.not().and(subresultat);
  let subresultat_partits = subresultat.and(rel_partit);
  let suma_escons = summarize_by_sum(subresultat_partits, 'vots')

  return prev.or(nopartits).or(new Relation(null, [{circumscripcio: circumscripcio_c, partit: partit_nou, vots: suma_escons}]));
  }

  return Array.from(votacio.values()).reduce(summarise, new Relation({circumscripcio: String, partit: String, vots: Number}) );
}


function ajunta_multiple(votacio, partits){
    //Ajunta partits en un partit_nou per cada circumscripció de la votació
    //Partits list string: ['A+B', 'C+D']
    function summarise(prev, partit){
	llista_partits = partit.split('+');
	return ajunta_partits(prev, llista_partits, partit);
    }
    return partits.reduce(summarise, votacio );
}


//parlament


function pesos(cens){
  //A partir de la taula de cens per circumscripció i escons de cada una calcula quin repartiment de pesos té cada circumscripció

  // summarize cens SUM(escons)
  let suma_escons = summarize_by_sum(cens, 'escons');
  // summarize cens SUM(cens)
  let suma_cens = summarize_by_sum(cens, 'cens');

  // ADD (escons/suma_escons)/(cens/suma_cens)
  // bug multiplicació i divisió floats
    let resultats = new Relation(null, Array.from(cens.values()).map(function taula(i){ return {circumscripcio: i['circumscripcio'], pes: (i['escons']/suma_escons)/(i['cens']/suma_cens)};}) )
  return resultats;
}


function barrera_legal(votacio, barrera){
  //A partir de vots a partits i en blanc (conjunt de vots vàlids) retorna els que passen la barrera legal, la qual és un paràmetre en tant per u. A més té en compte que els vots en blanc no s'han de retornar (es pot veure com que mai no passen la barrera legal), tot i que sí que s'utilitzen per fer el càlcul de la barrera legal

// select votacio where partit != 'nul'
let vots_nul = new Relation(null, [{partit: 'nul'}]);
let vots_valids = vots_nul.not().and(votacio);

// select vots_valids where partit != 'blanc'
let vots_blanc = new Relation(null, [{partit: 'blanc'}]);
let vots_candidatures = vots_blanc.not().and(vots_valids);

// summarize vots_valids SUM(vots)
let suma_vots_valids = summarize_by_sum(vots_valids, 'vots');
let vots_barrera = barrera*suma_vots_valids;

// select  vots_candidatures where vots >= vots_barrera
let rel_barrera = vots_candidatures.and(new Relation(null, [{y: vots_barrera}]));
let rel_comparacio = gte.and(vots_candidatures.and(rel_barrera).rename({ vots: 'x' }));
let passen = rel_comparacio.rename({x: 'vots'}).remove(['y']);

return passen;
}



function n_max_escons(quocients, n_escons){
  // summarize quocients NMAX(z)
  function nmax(prevmax, tup){
    // prevmax array de N màxims previs
    let nmax_min = Math.min.apply(null, prevmax);
    let candidate = tup['z'];
    if (candidate > nmax_min) {
      var index = prevmax.indexOf(nmax_min);
      prevmax.splice(index, 1);
      prevmax.push(candidate);
    }
    return prevmax
  }

  let quocients_n_max = Array.from(quocients.values()).reduce(nmax, Array(n_escons).fill(0));
  let quocient_n_max_min = Math.min.apply(null, quocients_n_max);

  // select quocients where z >= quocient_n_max_min
  let escons = {};
  function quocient2esco(tup){
    let partit = tup['partit'];
    let quocient = tup['z'];
    let esco = 0;
    if (quocient >= quocient_n_max_min) {esco = 1;}
    if (escons.hasOwnProperty(partit)) { escons[partit] = escons[partit] + esco }
    else {escons[partit] = esco;};
}

  quocients.forEach(quocient2esco);

  let escons_rel = new Relation(null, Object.entries(escons).map(function taula(i){ return {partit: i[0], vots: i[1]};}) )
  return escons_rel;
}


function Hondt(votacio, n_escons){
    //A partir de partits i vots que ha obtingut cada un, aplica la llei d'Hondt per repartir el nombre d'escons que toca a cada partit
    let divisor = new Relation(null, Array(n_escons).fill().map(function(e,i){return {y: parseInt(100*n_escons/(i+1))}}));
  // quocients bug divide per floats
  let quocients = multiply.compose( votacio.and(divisor).rename({vots: 'x'}) );

  // summarize quocients NMAX(z)
  // select quocients where z >= quocient_n_max_min
  let escons_rel = n_max_escons(quocients, n_escons);

  return escons_rel;
}


function Proporcional(votacio, n_escons){
    //A partir de partits i vots que ha obtingut cada un, aplica la llei Proporcional per repartir el nombre d'escons que toca a cada partit
    // El resultat pot no sumar el total de n_escons

    // summarize votacio SUM(vots)
    let suma_vots = summarize_by_sum(votacio, 'vots');
    // factor proporcional
    //let factor = new Relation(null, [{y: n_escons/suma_vots}]);
    // extend escons=vots*factor
    //let resultats = multiply.compose( votacio.and(factor).rename({vots: 'x'}) );
    // bug multiplicació floats
    let factor = n_escons/suma_vots;
    let resultats = new Relation(null, Array.from(votacio.values()).map(function taula(i){ return {partit: i['partit'], vots: parseInt(i['vots']*factor)};}) )
    return resultats;
}

function Hare(votacio, n_escons){
    //A partir de partits i vots que ha obtingut cada un, aplica la llei Hare/Niemeyer per repartir el nombre d'escons que toca a cada partit

    // summarize votacio SUM(vots)
    let vots_valids =  summarize_by_sum(votacio, 'vots');
    let quota = parseInt(vots_valids/n_escons);
    let quota_rel = new Relation(null, [{y: quota}]);

    // esconsquota = votacio/quota
    let escons_per_quota = new Relation(null, Array.from(votacio.values()).map(function taula(i){ return {partit: i['partit'], circumscripcio: i['circumscripcio'], vots: parseInt(i['vots']/quota)};}) )

    // vots de residu = vots - quota * escons per quota
    let residus = multiply.compose( escons_per_quota.and(quota_rel).rename({vots: 'x'}) );
    residus = subtract.compose(votacio.and(residus).rename({vots: 'x', z: 'y'}));

    // assigna escons per residu als majors vots de residus
    let escons_lliures = n_escons - summarize_by_sum(escons_per_quota, 'vots');
    let escons_per_residu = n_max_escons(residus, escons_lliures);

    // escons = escons per quota + escons per residu
    escons = plus.compose(escons_per_quota.rename({vots: 'x'}).and(escons_per_residu).rename({vots: 'y'}));
    return escons.rename({z: 'vots'});
}



function llei_circumscripcions(votacio, barrera, escons, llei=Hondt){
   //Aplica la fórmula electoral: escons per circumscripcions a on a cada una hi ha barrera legal més llei de distribució

function llei_distribucio(prev, circ){
  let circ_c = circ['circumscripcio'];
  // select votacio where circumscripcio = circ
  let votacio_barrera = votacio.and(new Relation(null, [{circumscripcio: circ_c}]));
  let rel_barrera = barrera_legal(votacio_barrera, barrera);
  let rel_hondt = llei(rel_barrera, circ['escons']);
  let resultat = rel_hondt.and(new Relation(null, [{circumscripcio: circ_c}]));
  return prev.or(resultat);
};

return Array.from(escons.values()).reduce(llei_distribucio, new Relation({circumscripcio: String, partit: String, vots: Number}) );

}


function total_partit(resultat){
   // SUMMARIZE sum(escons) PER partit

  function summarise(prev, partit){
  let partit_p = partit['partit'];
  // select resultat where partit = 'partit'
  let rel_partit = new Relation(null, [{partit: partit_p}]);
  let subresultat = rel_partit.and(resultat);
  let suma_escons = summarize_by_sum(subresultat, 'vots')

  return prev.or(new Relation(null, [{circumscripcio: 'unica', partit: partit_p, vots: suma_escons}]));
  }

  return Array.from(resultat.values()).reduce(summarise, new Relation({circumscripcio: String, partit: String, vots: Number}) );

}



function formula_electoral(votacio, circumscripcions, barrera, escons, llei){
    //Aplica la fórmula electoral a la votacio
    //1. Circumscripcions(si|no|pes) -> si: circumscripcions, no: única, pes: única corregint per pesos diferencials
    //Aplica per circumscripció el següent:
    //2. Barrera(enter tant per u): Aplica la barrera electoral
    //3. llei(str o function): Llei de distribució per total de escons
    if (circumscripcions=='pes'){
	taula_pesos = pesos(escons);
	// votacio JOIN taula_pesos ADD vots=pes*vots
	votacio = votacio.and(taula_pesos);
	// bug multiplicació floats
        votacio = new Relation(null, Array.from(votacio.values()).map(function taula(i){ return {circumscripcio: i['circumscripcio'], partit: i['partit'], vots: parseInt(i['vots']*i['pes'])};}) )
	circumscripcions='no';
    }
    if (circumscripcions=='no'){
      votacio = total_partit(votacio);
      escons = new Relation(null, [{circumscripcio: 'unica', escons: summarize_by_sum(escons, 'escons')}])
    }

    if (typeof llei == 'string'){
    	if (llei=='hondt'){llei = Hondt;}
	else if (llei=='proporcional'){llei = Proporcional;}
	else if (llei=='hare'){llei = Hare;}
    }

    return llei_circumscripcions(votacio, barrera, escons, llei);
}



function llei_actual(votacio, barrera, escons){
   //Aplica la fórmula electoral que hi ha actualment: escons per circumscripcions a on a cada una hi ha barrera legal més llei d'Hondt

    return  total_partit(formula_electoral(votacio, 'si', barrera, escons, Hondt));

}


