var assert = chai.assert;


//exemples
let votacio_exemple1 = new Relation(null, [{partit: 'FIU', vots: 10}, {partit: 'REC', vots: 5}, {partit: 'PUC', vots: 1}]);





describe('Llei Hondt', function() {
  describe('Exemple 1', function() {
    it("votacio={'FIU': 10, 'REC': 5, 'PUC': 1,} --> Hondt(votacio,4) == {'FIU': 3, 'REC': 1,'PUC': 0}", function() {
	let votacio = votacio_exemple1;
	let resultat = Hondt(votacio, 4);
	let resultat_ass = new Relation(null, [{partit: 'FIU', vots: 3}, {partit: 'REC', vots: 1}, {partit: 'PUC', vots: 0}]);
	assert.deepEqual(resultat, resultat_ass);
    });
  });

  describe('Exemple 2. parlament bcn 2012', function() {
      it("--> Hondt(votacio,85) == {'CIU': 26, 'ERC': 12,'PSC': 14,...}", function() {
       let votacio = new Relation(null, [
        {partit: 'CiU', vots: 762628},
        {partit: 'ERC', vots: 345586},
        {partit: 'PSC', vots: 418847},
        {partit: 'PP', vots: 361581},
        {partit: 'ICV', vots: 303013},
        {partit: 'Cs', vots: 229725},
        {partit: 'CUP', vots: 92621},
	  ]);
	let resultat = Hondt(votacio, 85);
	  let resultat_ass = new Relation(null, [
        {partit: 'CiU', vots: 26},
        {partit: 'ERC', vots: 12},
        {partit: 'PSC', vots: 14},
        {partit: 'PP', vots: 12},
        {partit: 'ICV', vots: 10},
        {partit: 'Cs', vots: 8},
        {partit: 'CUP', vots: 3},
	  ]);
	assert.deepEqual(resultat, resultat_ass);
    });
  });

  describe('Exemple 3. parlament lleida 2012', function() {
      it("--> Hondt(votacio,15) == {'CIU': 8, 'ERC': 3,'PSC': 1,...}", function() {
       let votacio = new Relation(null, [
        {partit: 'CiU', vots: 89035},
        {partit: 'ERC', vots: 36011},
        {partit: 'PSC', vots: 21598},
        {partit: 'PP', vots: 23338},
        {partit: 'ICV', vots: 11145},
        {partit: 'Cs', vots: 6881},
        {partit: 'CUP', vots: 6302},
	  ]);
	let resultat = Hondt(votacio, 15);
	  let resultat_ass = new Relation(null, [
        {partit: 'CiU', vots: 8},
        {partit: 'ERC', vots: 3},
        {partit: 'PSC', vots: 1},
        {partit: 'PP', vots: 2},
        {partit: 'ICV', vots: 1},
        {partit: 'Cs', vots: 0},
        {partit: 'CUP', vots: 0},
	  ]);
	assert.deepEqual(resultat, resultat_ass);
    });
  });

    
});




describe('Llei Proporcional', function() {

  describe('Exemple 1', function() {
    it("votacio={'FIU': 10, 'REC': 5, 'PUC': 1,} --> Proporcional(votacio,4) == {'FIU': 3, 'REC': 1,'PUC': 0}", function() {
	let votacio = votacio_exemple1;
	let resultat = Proporcional(votacio, 4);
	let resultat_ass = new Relation(null, [{partit: 'FIU', vots: 2}, {partit: 'REC', vots: 1}, {partit: 'PUC', vots: 0}]);
	assert.deepEqual(resultat, resultat_ass);
    });
  });

});




describe('Llei actual', function() {
  describe('Parlament 2012', function() {
    it("--> llei_actual(vots_candidatures, 0.03, rel_circumscripcions) == {'CIU': 50, 'ERC': 21, 'PSC': 20, 'PP': 19, 'ICV': 13, 'Cs': 9, 'CUP': 3}", function() {


  let escons_circumscripcions = new Relation(null, [{ circumscripcio: "barcelona", escons: 85 }, { circumscripcio: "girona", escons:  17 }, { circumscripcio: "tarragona", escons: 18 }, { circumscripcio: "lleida", escons: 15 }])

let cens_circumscripcions = new Relation(null, [{ circumscripcio: "barcelona", cens: 3921952 }, { circumscripcio: "girona", cens:  490346 }, { circumscripcio: "tarragona", cens: 545122 }, { circumscripcio: "lleida", cens: 300540 }])


let vots_candidatures_bcn = new Relation(null, [
        {partit: 'CiU', vots: 762628},
        {partit: 'ERC', vots: 345586},
        {partit: 'PSC', vots: 418847},
        {partit: 'PP', vots: 361581},
        {partit: 'ICV', vots: 303013},
        {partit: 'Cs', vots: 229725},
        {partit: 'CUP', vots: 92621},
        {partit: 'PxC', vots: 51449},
        {partit: 'SI', vots: 32141},
        {partit: 'Eb', vots: 22404},
        {partit: 'PACMA', vots: 16402},
        {partit: 'Pirata.cat', vots: 14980},
        {partit: 'UPyD', vots: 12106},
        {partit: 'FARTS.cat', vots: 9167},
        {partit: 'VD', vots: 5936},
        {partit: 'UCE', vots: 1927},
        {partit: 'blanc', vots: 38035},
        {partit: 'nul', vots: 22600}
])

let vots_candidatures_gir = new Relation(null, [
        {partit: 'CiU', vots: 147827},
        {partit: 'ERC', vots: 61020},
        {partit: 'PSC', vots: 34567},
        {partit: 'PP', vots: 32192},
        {partit: 'ICV', vots: 20317},
        {partit: 'Cs', vots: 12325},
        {partit: 'CUP', vots: 14505},
        {partit: 'PxC', vots: 3378},
        {partit: 'SI', vots: 5986},
        {partit: 'Eb', vots: 1957},
        {partit: 'PACMA', vots: 1578},
        {partit: 'Pirata.cat', vots: 1133},
        {partit: 'UPyD', vots: 696},
        {partit: 'FARTS.cat', vots: 711},
        {partit: 'UCE', vots: 201},
        {partit: 'PRE-IR', vots: 517},
        {partit: 'blanc', vots: 4315},
        {partit: 'nul', vots: 2766}
])

let vots_candidatures_lld = new Relation(null, [
        {partit: 'CiU', vots: 89035},
        {partit: 'ERC', vots: 36011},
        {partit: 'PSC', vots: 21598},
        {partit: 'PP', vots: 23338},
        {partit: 'ICV', vots: 11145},
        {partit: 'Cs', vots: 6881},
        {partit: 'CUP', vots: 6302},
        {partit: 'PxC', vots: 1224},
        {partit: 'SI', vots: 3038},
        {partit: 'Eb', vots: 1417},
        {partit: 'PACMA', vots: 752},
        {partit: 'Pirata.cat', vots: 642},
        {partit: 'UPyD', vots: 393},
        {partit: 'FARTS.cat', vots: 476},
        {partit: 'PRE-IR', vots: 301},
        {partit: 'UCE', vots: 90},
        {partit: 'blanc', vots: 4153},
        {partit: 'nul', vots: 2347}
])

let vots_candidatures_tar = new Relation(null, [
        {partit: 'CiU', vots: 113250},
        {partit: 'ERC', vots: 53853},
        {partit: 'PSC', vots: 48453},
        {partit: 'PP', vots: 53435},
        {partit: 'ICV', vots: 24448},
        {partit: 'Cs', vots: 25998},
        {partit: 'CUP', vots: 12807},
        {partit: 'PxC', vots: 4094},
        {partit: 'SI', vots: 5462},
        {partit: 'Eb', vots: 2105},
        {partit: 'PACMA', vots: 2053},
        {partit: 'Pirata.cat', vots: 1195},
        {partit: 'UPyD', vots: 1360},
        {partit: 'FARTS.cat', vots: 1321},
        {partit: 'SiR', vots: 332},
        {partit: 'UCE', vots: 285},
        {partit: 'blanc', vots: 6394},
        {partit: 'nul', vots :4521}
])

let circ_bcn = vots_candidatures_bcn.and(new Relation(null, [{circumscripcio: "barcelona"}]))
let circ_gir = vots_candidatures_gir.and(new Relation(null, [{circumscripcio: "girona"}]))
let circ_lld = vots_candidatures_lld.and(new Relation(null, [{circumscripcio: "lleida"}]))
let circ_tar = vots_candidatures_tar.and(new Relation(null, [{circumscripcio: "tarragona"}]))

let vots_candidatures = circ_bcn.or(circ_gir).or(circ_lld).or(circ_tar)
let rel_circumscripcions = escons_circumscripcions.and(cens_circumscripcions);

let resultat = llei_actual(vots_candidatures, 0.03, rel_circumscripcions)
let resultat_ass = new Relation(null, [
        {partit: 'CiU', vots: 50},
        {partit: 'ERC', vots: 21},
        {partit: 'PSC', vots: 20},
        {partit: 'PP', vots: 19},
        {partit: 'ICV', vots: 13},
        {partit: 'Cs', vots: 9},
        {partit: 'CUP', vots: 3},
	  ]);

assert.deepEqual(resultat.project(['partit', 'vots']), resultat_ass);
	

	
    });
  });

    
});


	

