Simulador Electoral Parlament
=============================

|Test|

Una aplicació per canviar la llei electoral i experimentar quins canvis hi hauria en els escons assignats a les candidatures segons la fórmula electoral escollida. Eleccions al Parlament de Catalunya. https://parlament.timbaler.cat

Ha estat programat orientat a dades relacionals mitjançant la llibreria `Relational.js <https://github.com/erikolson186/relational.js>`_.

Per als gràfics utilitza la llibreria `Plotly.js <https://plotly.com/javascript/>`_.


.. |Test| image:: https://gitlab.com/timbaler/parlament/badges/master/pipeline.svg
        :target: https://gitlab.com/timbaler/parlament/commits/master


Developing
----------

Install requirement and launch tests::

   npm install relational.js
   npm install browserify
   ../node_modules/browserify/bin/cmd.js -r relational.js -o bundle-relational.js
   test_parlament.html


License
-------

``parlament`` is offered under the GPLv3 license.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
